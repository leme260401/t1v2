using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour
{
    public float velocityX = 10;
    public float jumpForce = 50;
    
private Rigidbody2D rb;
private Animation _animator;



// Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocityX, rb.velocity.y);
        
        
        if (Input.GetKeyUp(KeyCode.Space))
        {
          rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        if (Input.GetKey(KeyCode.C))
        {

            -_animator.SetInteger("Estado", 1);
        }
      
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject .gameObject.CompareTag("Zombie") && isSLide)
        {Destroy(other.gameObject);
        }
        else if (other. gameObject.CompareTag("Zombie" && isSLide))
        {
            Debug.Log("Morir");
            _animator.SetInteger("Estado", -1);
        }
    }
}
